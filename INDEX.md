# MKEYB

Very small Keyboarddriver, 500-700 Bytes resident


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## MKEYB.LSM

<table>
<tr><td>title</td><td>MKEYB</td></tr>
<tr><td>version</td><td>0.46</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-10-09</td></tr>
<tr><td>description</td><td>Very small Keyboarddriver, 500-700 Bytes resident</td></tr>
<tr><td>keywords</td><td>freedos keyboard</td></tr>
<tr><td>author</td><td>Tom Ehlert, Email found at website</td></tr>
<tr><td>maintained&nbsp;by</td><td>Tom Ehlert</td></tr>
<tr><td>primary&nbsp;site</td><td>http://www.drivesnapshot.de/freedos/mkeyb.htm</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.drivesnapshot.de/freedos/mkeyb.htm</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
</table>
